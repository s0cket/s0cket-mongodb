//========================================================================================
//  mongodb.js
//
//  This module is to perform CRUD operations with mongo db.
//
//  Company: Bizar Mobile
//  Date: 18/05/2015
//  Version: 0.0.4
//
//========================================================================================
var Db = require('mongodb').Db;
var ReplSet = require('mongodb').ReplSet;
var MongoClient = require('mongodb').MongoClient;
// Load database config based on server enviornment mode.
var db = {};
//--------------------------------------------------------------------------------------------------------------
//  connect
//
//  This function connects to the database
//  Call: db = dbm.connect();
//--------------------------------------------------------------------------------------------------------------
exports.connect = function(config, callback) {
    var url = "mongodb://";
    if (config.db.replSet) {
        var replSet = config.db.replSet;
        url += replSet.hosts.join() + "/" + config.db.name + "?w=1";
        if (replSet.name) url += "&replicaSet=" + replSet.name;
        if (replSet.readPreference) url += "&readPreference=" + replSet.readPreference;
    } else {
        url += config.db.host + ":" + config.db.port + "/" + config.db.name;
    }
    MongoClient.connect(url, function(err, _db) {
        if (err) {
            console.log(err);
        } else {
            console.log('Connected to mongodb: ' + config.db.name + " at " + url);
            db = _db;
        }
        if (callback) callback(err, config.db.name);
    });
};
//--------------------------------------------------------------------------------------------------------------
//  isConnected
//
//  This function returns the status of the database connection
//  Call: db = dbm.isConnected();
//--------------------------------------------------------------------------------------------------------------
exports.isConnected = function() {
    if (_.isEmpty(db)) {
        return false;
    }
    return true;
};
//--------------------------------------------------------------------------------------------------------------
//  count
//
//  This function queries the database and returns the result set
//--------------------------------------------------------------------------------------------------------------
exports.count = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.count(params.find, function(err, count) {
            callback(err, count);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  find
//
//  This function queries the database and returns the result set
//  Add filter to allow for soft deletes. Use findAll to list deleted
//  documents as well
//--------------------------------------------------------------------------------------------------------------
exports.find = function(params, callback) {
    params.find.deleted = {
        $exists: false
    };
    db.collection(params.collection, function(err, collection) {
        var cursor = collection.find(params.find, params.fields ? params.fields : []);
        if (params.sort) {
            cursor.sort(params.sort);
        }
        if (params.skip) {
            cursor.skip(params.skip);
        }
        if (params.limit) {
            cursor.limit(params.limit);
        }
        cursor.toArray(function(err, res) {
            callback(err, res);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  findAll
//
//  This function queries the database and returns the result set
//--------------------------------------------------------------------------------------------------------------
exports.findAll = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        var cursor = collection.find(params.find, params.fields ? params.fields : []);
        if (params.sort) {
            cursor.sort(params.sort);
        }
        if (params.skip) {
            cursor.skip(params.skip);
        }
        if (params.limit) {
            cursor.limit(params.limit);
        }
        cursor.toArray(function(err, res) {
            callback(err, res);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  distinct
//
//  This function queries the database and returns the result set with distinct values
//--------------------------------------------------------------------------------------------------------------
exports.distinct = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        var cursor = collection.distinct(params.distinct, params.query ? params.query : []);
        cursor.toArray(function(err, res) {
            callback(err, res);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  findAndModify
//
//  This function updates the desired document and returns back the updated object
//--------------------------------------------------------------------------------------------------------------
exports.findAndModify = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.findAndModify(
        params.findAndModify, 
        params.sort, 
        params.update, 
        params.options, 
        function(err, object) {
            callback(err, object);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  findOne
//
//  This function find one document as per the passed findOne json object.
//--------------------------------------------------------------------------------------------------------------
exports.findOne = function(params, callback) {
    var fields = params.fields || {};
    db.collection(params.collection, function(err, collection) {
        collection.findOne(
        params.findOne, 
        fields, 
        function(err, object) {
            callback(err, object);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  findOneWeb
//
//  This function find one document as per the passed findOne json object.
//--------------------------------------------------------------------------------------------------------------
exports.findOneWeb = function(req, res, params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.findOne(
        params.findOne, function(err, object) {
            callback(req, res, params, object);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  insert
//
//  This function inserts a document into a collection
//--------------------------------------------------------------------------------------------------------------
exports.insert = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.insert(params.insert, {
            w: 1
        }, function(err, objects) {
            if (err) console.warn(err.message);
            if (err && err.message.indexOf('E11000 ') !== -1) {}
            if (callback) {
                callback(err, objects);
            }
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  update
//
//  This function updates a document based on the update query
//--------------------------------------------------------------------------------------------------------------
exports.update = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.update(
        params.update, 
        params.objNew, 
        params.options, 
        function(err, object) {
            if (callback) {
                callback(err, object);
            }
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  remove
//
//  This function queries the database and deletes the result set
//--------------------------------------------------------------------------------------------------------------
exports.remove = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.remove(params.remove, function(err, res) {
            callback(err, res);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  softDelete
//
//  Adds a deleted flag to the document so it doesn't get selected by find()
//--------------------------------------------------------------------------------------------------------------
exports.softDelete = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.update(
        params.softDelete, 
        { deleted: Date.now() }, 
        params.options, 
        function(err, object) {
            if (callback) {
                callback(err, object);
            }
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  aggregate
//
//  This function queries the database and agrregates the result set
//--------------------------------------------------------------------------------------------------------------
exports.aggregate = function(params, callback) {
    db.collection(params.collection, function(err, collection) {
        collection.aggregate(params.aggregate, function(err, res) {
            callback(err, res);
        });
    });
};
//--------------------------------------------------------------------------------------------------------------
//  getNextSequence
//
//  This function auto increments a sequence and returns the updated object.
//--------------------------------------------------------------------------------------------------------------
exports.getNextSequence = function(sequenceName, callback) {
    console.log('getNextSequence');
    db.collection('sequenceCollection', function(err, collection) {
        console.log('getNextSequence2:' + sequenceName);
        collection.findAndModify({
            _id: sequenceName
        }, {}, {
            $inc: {
                seq: 1
            }
        }, {
            upsert: true
        }, function(err, object) {
            console.log(err);
            console.log(JSON.stringify(object));
            callback(err, object);
        });
    });
};
//==============================================================================================================
//  mongodb.js
//==============================================================================================================